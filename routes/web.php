<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('admin.dashboard');
});

Route::get('/product-list', [ProductController::class, 'index'])->name('product-list');
Route::get('/datatable', [ProductController::class, 'dataTable'])->name('datatable');

Route::get('/create', [ProductController::class, 'create'])->name('create');
Route::get('/update/{id}', [ProductController::class, 'edit'])->name('update');

Route::post('/store', [ProductController::class, 'store'])->name('store');
Route::patch('/update/{id}', [ProductController::class, 'update']);

Route::delete('delete', [ProductController::class, 'delete'])->name('delete');
