<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Models\Products;

use Auth;
use DataTables; 
use Response;
use DB;



class ProductController extends Controller
{
    private $view = 'admin.';
    
    public function index()
    {
        //return view($this->view.'index')->with(compact('districts'));
        return view($this->view.'products');
    }
    
    public function dataTable(Request $request)
    {
        //dd('ss');
        $columns = array( 
            0 => 'title',
            1 => 'description',
            2 => 'image',
            3 => 'size',
            4 => 'color',
        );

        
        $limit = $request->length;
        $start = $request->start;
        $order = isset($request->order) ? $columns[$request->order[0]['column']] : 'created_at';
        $dir = (isset($request->order) && $request->order[0]['dir'] == 'asc') ? "ASC" : "DESC";
        $searchVal = $request->search['value'];

        $couponplans = Products::select('*')->whereNull('deleted_at');
        if(!empty($searchVal)) {
            $couponplans = $couponplans ->where(function ($qry) use ($searchVal) {
                $qry->orWhere('title', 'LIKE', '%'.$searchVal.'%');
                $qry->orWhere('description', 'LIKE', '%'.$searchVal.'%');
                $qry->orWhere('size', 'LIKE', '%'.$searchVal.'%');
                $qry->orWhere('color', 'LIKE', '%'.$searchVal.'%');
            });
        }
        $count = $couponplans
                ->count();

        $result['data'] = $couponplans->skip(intval($start))
                ->limit(intval($limit))
                // ->options([
                //     'collation' => [
                //         'locale' => 'en',
                //         'strength' => 2
                //     ]
                // ])
                ->orderBy($order,$dir)
                ->get();

        $result['recordsTotal'] = intval($count);
        $result['recordsFiltered'] =  intval($count);
        return response()->json($result);
    }

    public function create()
    {
        return view($this->view.'createOrUpdate');
    }

    public function edit($id)
    {
        $product = Products::find($id);
        return view($this->view.'createOrUpdate')->with('product', $product);
    }

    public function store(Request $request)
    {
        
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
            'size' => 'required',
            'color' => 'required',
        ]);

        $post = new Products;
        $post->title = $request->input('title');
        $post->description = $request->input('description');
        $post->size = $request->input('size');
        $post->color = $request->input('color');
        $post->save();

        return redirect('product-list');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
            'size' => 'required',
            'color' => 'required',
        ]);

        $post = Products::where('id', $id)->firstOrFail();

        $post->title = $request->input('title');
        $post->description = $request->input('description');
        $post->size = $request->input('size');
        $post->color = $request->input('color');
        $post->save();

        return redirect('product-list');
    }

    public function delete(Request $request)
    {
        $post = Products::where('id', $request->id)->firstOrFail();

        $post->delete();

        if($post) return Response::json(['status' => true, 'msg' => "deleted successfully."]);
        else return Response::json(['status' => false, 'msg' => "Something went wrong. Please try again later."]);
    }
}