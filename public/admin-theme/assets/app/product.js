$(document).ready(function () {
    var url = $('#site-url').val();
    $(document).ready(function () {
        $('#js-product-list').DataTable({
            processing: true,
            serverSide: true,
            order: [],
            ajax: {
                url : url+'/datatable',
            },
            columns: [
                {data: 'title', name: 'title', class:'title'},
                {data: 'description', name: 'description', class:'description'},
                {data: 'image', name: 'image', class:'image'},
                {data: 'size', name: 'size', class:'size'},
                {data: 'color', name: 'color', class:'color'},
            ],
            "aoColumnDefs": [
                {
                     "aTargets": [5],
                     "mData": "id",
                     "mRender": function (data, type, full) {
                         //<button class='btn btn-label-primary btn-icon js-delete-coupon-plan' data-index="+data+"> <i class='fa fa-trash'></i> </button>
                        return "<a href='"+ url +"/update/"+ data +"'><button class='btn btn-label-primary btn-icon mr-1 js-edit-'> <i class='fa fa-edit'></i></button></a><button class='btn btn-label-primary btn-icon js-delete-product' data-index="+data+"> <i class='fa fa-trash'></i> "
                     }
                 }
              ]
        });
    });
});

$(document).on('click', '#js-product-list .js-delete-product', function() {
    var id = $(this).attr('data-index');
    var url = $('#site-url').val();
    Swal.fire({
        title: 'Are you sure, do you want to delete this document?',
        text: "You won't be able to revert this!",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url: url+'/delete?id=' + id,
                type: "DELETE",
                dataType: "json",
                success: function(result) {
                    $("#js-product-list").DataTable().ajax.reload();
                    setTimeout(function() {
                        $('#common-alert').remove();
                    }, 1000);
                }
            });
        }
    });

});