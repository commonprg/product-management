@include('admin.layouts.header')
@include('admin.layouts.sidebar')

<!-- BEGIN: Content Dynamically -->
@yield('content')
<!-- END: Content -->

@include('admin.layouts.footer')
</div>
      <!-- END Page Wrapper -->
    </div>
    <!-- END Page Holder -->
    <!-- BEGIN Scroll To Top -->
    <div class="scrolltop">
      <button class="btn btn-info btn-icon btn-lg">
        <i class="fa fa-angle-up"></i>
      </button>
    </div>
    <!-- END Scroll To Top -->
  
    <!-- BEGIN Float Button -->
    <div class="float-btn float-btn-right">
      <button
        class="btn btn-flat-primary btn-icon mb-2"
        id="theme-toggle"
        data-toggle="tooltip"
        data-placement="right"
        title="Change theme"
      >
        <i class="fa fa-moon"></i>
      </button>
      <a
        href="#"
        class="btn btn-flat-primary btn-icon"
        data-toggle="tooltip"
        data-placement="right"
        title="Switch to RTL"
      >
        <i class="fa fa-sync"></i>
      </a>
    </div>
    <!-- END Float Button -->
    <script type="text/javascript" src="{{ asset('admin-theme/assets/build/scripts/mandatory.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-theme/assets/build/scripts/core.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-theme/assets/build/scripts/vendor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin-theme/assets/app/home.js') }}"></script>

        <!-- begin::Custom Js files include -->

        <!-- begin::Success Toastr Common Script -->
        @if($message = Session::get('success'))
            <script type="text/javascript">
                toastr.success("{{ $message }}", "Success", {
                    progressBar: !0
                })
            </script>
        @endif
        @if($message = Session::get('error'))
            <script type="text/javascript">
                toastr.error("{{ $message }}", "Fail", {
                    progressBar: !0
                })
            </script>
        @endif
        <!-- END: Success Toastr Common Script -->
        @stack('js')
        <script src="{{ mix('js/app.js') }}"></script>
        <!-- <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script> -->
        @stack('scripts')
  </body>
</html>

    
