<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link
      href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600/family=Roboto+Mono/display=swap"
      rel="stylesheet"
/>
    <link href="{{ asset('admin-theme/assets/build/styles/ltr-core.css') }}" rel="stylesheet" />
    <link href="{{ asset('admin-theme/assets/build/styles/ltr-vendor.css') }}" rel="stylesheet" />
    <link
      href="{{ asset('admin-theme/assets/images/favicon.ico') }}"
      rel="shortcut icon"
      type="image/x-icon"
    />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">  
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/css/dataTables.checkboxes.css" rel="stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <input type="hidden" value="{{url('/')}}" id="site-url"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Dashboard | Panely</title>
    <style>
      .dataTables_length select {
        width: 50px !important;
      }
    }
    </style>
  </head>

  <body
    class="
      theme-light
      preload-active
      aside-active aside-mobile-minimized aside-desktop-maximized
    "
    id="fullscreen" style="font-family: 'Poppins', sans-serif;"
  >
    <!-- BEGIN Preload -->
    <div class="preload">
      <div class="preload-dialog">
        <!-- BEGIN Spinner -->
        <div class="spinner-border text-primary preload-spinner"></div>
        <!-- END Spinner -->
      </div>
    </div>
    <!-- END Preload -->
    <!-- BEGIN Page Holder -->
    <div class="holder">
      <!-- BEGIN Aside -->
      <div class="aside">
        <div class="aside-header">
          <h3 class="aside-title">Panely</h3>
          <div class="aside-addon">
            <button
              class="btn btn-label-primary btn-icon btn-lg"
              data-toggle="aside"
            >
              <i class="fa fa-times aside-icon-minimize"></i>
              <i class="fa fa-thumbtack aside-icon-maximize"></i>
            </button>
          </div>
        </div>
        <div class="aside-body" data-simplebar="data-simplebar">
          <!-- BEGIN Menu -->
          <div class="menu">
            <div class="menu-item">
              <a
                href="{{ asset('admin/ltr/index.html') }}"
                data-menu-path="{{ asset('admin-theme/ltr/index.html') }}"
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-desktop"></i>
                </div>
                <span class="menu-item-text">Dashboard</span>
                <div class="menu-item-addon">
                  <span class="badge badge-success">New</span>
                </div>
              </a>
            </div>
            <!-- BEGIN Menu Section -->
            <div class="menu-section">
              <div class="menu-section-icon">
                <i class="fa fa-ellipsis-h"></i>
              </div>
              <h2 class="menu-section-text">Elements</h2>
            </div>
            <!-- END Menu Section -->
            <div class="menu-item">
              <button class="menu-item-link menu-item-toggle">
                <div class="menu-item-icon">
                  <i class="fa fa-palette"></i>
                </div>
                <span class="menu-item-text">Base</span>
                <div class="menu-item-addon">
                  <i class="menu-item-caret caret"></i>
                </div>
              </button>
              <!-- BEGIN Menu Submenu -->
              <div class="menu-submenu">
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/accordion.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/accordion.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Accordion</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/alert.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/alert.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Alert</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/badge.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/badge.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Badge</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/button.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/button.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Button</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/button-group.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/button-group.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Button group</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/card.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/card.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Card</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/color.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/color.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Color</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/dropdown.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/dropdown.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Dropdown</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/grid-nav.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/grid-nav.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Grid navigation</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/list-group.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/list-group.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">List group</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/marker.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/marker.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Marker</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/modal.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/modal.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Modal</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/nav.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/nav.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Navigation</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/pagination.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/pagination.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Pagination</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/popover.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/popover.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Popover</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/progress.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/progress.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Progress</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/spinner.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/spinner.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Spinner</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/tab.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/tab.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Tab</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/table.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/table.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Table</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/tooltip.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/tooltip.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Tooltip</span>
                  </a>
                </div>
                <div class="menu-item">
                  <a
                    href="{{ asset('admin-theme/ltr/elements/base/type.html') }}"
                    data-menu-path="{{ asset('admin-theme/ltr/elements/base/type.html') }}"
                    class="menu-item-link"
                  >
                    <i class="menu-item-bullet"></i>
                    <span class="menu-item-text">Typography</span>
                  </a>
                </div>
              </div>
              <!-- END Menu Submenu -->
            </div>
          </div>
          <!-- END Menu -->
        </div>
      </div>
      <!-- END Aside -->
      <!-- BEGIN Page Wrapper -->
      <div class="wrapper">
        <!-- BEGIN Header -->
        <div class="header">
          <!-- BEGIN Header Holder -->
          <div
            class="header-holder header-holder-desktop sticky-header"
            id="sticky-header-desktop"
          >
            <div class="header-container container-fluid">
              <div class="header-wrap">
                <!-- BEGIN Nav -->
                <!-- END Nav -->
              </div>
              <div class="header-wrap header-wrap-block">
                <!-- BEGIN Input Group -->
                <!-- END Input Group -->
              </div>
              <div class="header-wrap">
                <!-- BEGIN Dropdown -->
                <div class="dropdown ml-2">
                  <button
                    class="btn btn-flat-primary widget13"
                    data-toggle="dropdown"
                  >
                    <div class="widget13-text">Hi <strong>User</strong></div>
                    <!-- BEGIN Avatar -->
                    <div class="avatar avatar-info widget13-avatar">
                      <div class="avatar-display">
                        <i class="fa fa-user-alt"></i>
                      </div>
                    </div>
                    <!-- END Avatar -->
                  </button>
                  <div
                    class="
                      dropdown-menu
                      dropdown-menu-wide
                      dropdown-menu-right
                      dropdown-menu-animated
                      overflow-hidden
                      py-0
                    "
                  >
                    <!-- BEGIN Portlet -->
                    <div class="portlet border-0">
                      <div class="portlet-header bg-primary rounded-0">
                        <!-- BEGIN Rich List Item -->
                        <div class="rich-list-item w-100 p-0">
                          <div class="rich-list-prepend">
                            <!-- BEGIN Avatar -->
                            <div class="avatar avatar-circle">
                              <div class="avatar-display">
                                <img
                                  src="{{ asset('admin-theme/assets/images/avatar/blank.webp') }}"
                                  alt="Avatar image"
                                />
                              </div>
                            </div>
                            <!-- END Avatar -->
                          </div>
                          <div class="rich-list-content">
                            <h3 class="rich-list-title text-white">
                            </h3>
                            <span class="rich-list-subtitle text-white"
                              >Software Engineer</span
                            >
                          </div>
                        </div>
                        <!-- END Rich List Item -->
                      </div>
                      <div class="portlet-body p-0">
                        <!-- BEGIN Grid Nav -->
                        <div
                          class="
                            grid-nav
                            grid-nav-flush
                            grid-nav-action
                            grid-nav-no-rounded
                          "
                        >
                          <div class="grid-nav-row">
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-address-card"></i>
                              </div>
                              <span class="grid-nav-content">Profile</span>
                            </a>
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-comments"></i>
                              </div>
                              <span class="grid-nav-content">Messages</span>
                            </a>
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-clone"></i>
                              </div>
                              <span class="grid-nav-content">Activities</span>
                            </a>
                          </div>
                          <div class="grid-nav-row">
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-calendar-check"></i>
                              </div>
                              <span class="grid-nav-content">Tasks</span>
                            </a>
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-sticky-note"></i>
                              </div>
                              <span class="grid-nav-content">Notes</span>
                            </a>
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-bell"></i>
                              </div>
                              <span class="grid-nav-content">Notification</span>
                            </a>
                          </div>
                        </div>
                        <!-- END Grid Nav -->
                      </div>
                    </div>
                    <!-- END Portlet -->
                  </div>
                </div>
                <!-- END Dropdown -->
              </div>
            </div>
          </div>
          <!-- END Header Holder -->
          <!-- BEGIN Header Holder -->
          <div
            class="header-holder header-holder-mobile sticky-header"
            id="sticky-header-mobile"
          >
            <div class="header-container container-fluid">
              <div class="header-wrap">
                <button
                  class="btn btn-flat-primary btn-icon"
                  data-toggle="aside"
                >
                  <i class="fa fa-bars"></i>
                </button>
              </div>
              <div
                class="header-wrap header-wrap-block justify-content-start px-3"
              >
                <h4 class="header-brand">Panely</h4>
              </div>
              <div class="header-wrap">
                <!-- BEGIN Dropdown -->
                <div class="dropdown ml-2">
                  <button
                    class="btn btn-flat-primary widget13"
                    data-toggle="dropdown"
                  >
                    <div class="widget13-text">Hi <strong>User</strong></div>
                    <!-- BEGIN Avatar -->
                    <div class="avatar avatar-info widget13-avatar">
                      <div class="avatar-display">
                        <i class="fa fa-user-alt"></i>
                      </div>
                    </div>
                    <!-- END Avatar -->
                  </button>
                  <div
                    class="
                      dropdown-menu
                      dropdown-menu-wide
                      dropdown-menu-right
                      dropdown-menu-animated
                      overflow-hidden
                      py-0
                    "
                  >
                    <!-- BEGIN Portlet -->
                    <div class="portlet border-0">
                      <div class="portlet-header bg-primary rounded-0">
                        <!-- BEGIN Rich List Item -->
                        <div class="rich-list-item w-100 p-0">
                          <div class="rich-list-prepend">
                            <!-- BEGIN Avatar -->
                            <div class="avatar avatar-circle">
                              <div class="avatar-display">
                                <img
                                  src="{{ asset('admin-theme/assets/images/avatar/blank.webp') }}"
                                  alt="Avatar image"
                                />
                              </div>
                            </div>
                            <!-- END Avatar -->
                          </div>
                          <div class="rich-list-content">
                            <h3 class="rich-list-title text-white">
                            </h3>
                            <span class="rich-list-subtitle text-white"
                              >Software Engineer</span
                            >
                          </div>
                        </div>
                        <!-- END Rich List Item -->
                      </div>
                      <div class="portlet-body p-0">
                        <!-- BEGIN Grid Nav -->
                        <div
                          class="
                            grid-nav
                            grid-nav-flush
                            grid-nav-action
                            grid-nav-no-rounded
                          "
                        >
                          <div class="grid-nav-row">
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-address-card"></i>
                              </div>
                              <span class="grid-nav-content">Profile</span>
                            </a>
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-comments"></i>
                              </div>
                              <span class="grid-nav-content">Messages</span>
                            </a>
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-clone"></i>
                              </div>
                              <span class="grid-nav-content">Activities</span>
                            </a>
                          </div>
                          <div class="grid-nav-row">
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-calendar-check"></i>
                              </div>
                              <span class="grid-nav-content">Tasks</span>
                            </a>
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-sticky-note"></i>
                              </div>
                              <span class="grid-nav-content">Notes</span>
                            </a>
                            <a href="#" class="grid-nav-item">
                              <div class="grid-nav-icon">
                                <i class="far fa-bell"></i>
                              </div>
                              <span class="grid-nav-content">Notification</span>
                            </a>
                          </div>
                        </div>
                        <!-- END Grid Nav -->
                      </div>
                      <div
                        class="portlet-footer portlet-footer-bordered rounded-0"
                      >
                        <button class="btn btn-label-danger">Sign out</button>
                      </div>
                    </div>
                    <!-- END Portlet -->
                  </div>
                </div>
                <!-- END Dropdown -->
              </div>
            </div>
          </div>
          <!-- END Header Holder -->
          <!-- BEGIN Header Holder -->
          <div class="header-holder header-holder-mobile">
            <div class="header-container container-fluid">
              <div
                class="
                  header-wrap header-wrap-block
                  justify-content-start
                  w-100
                "
              >
              </div>
            </div>
          </div>
          <!-- END Header Holder -->
        </div>
        <!-- END Header -->