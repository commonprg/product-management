      <!-- BEGIN Aside -->
      <div class="aside">
        <div class="aside-header">
          <h3 class="aside-title">Panely</h3>
          <div class="aside-addon">
            <button
              class="btn btn-label-primary btn-icon btn-lg"
              data-toggle="aside"
            >
              <i class="fa fa-times aside-icon-minimize"></i>
              <i class="fa fa-thumbtack aside-icon-maximize"></i>
            </button>
          </div>
        </div>
        <div class="aside-body" data-simplebar="data-simplebar">
          <!-- BEGIN Menu -->
          <div class="menu">
            <div class="menu-item">
              <a
                href=""
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-desktop"></i>
                </div>
                <span class="menu-item-text">Dashboard</span>
                <div class="menu-item-addon">
                  <span class="badge badge-success">New</span>
                </div>
              </a>
            </div>
            <div class="menu-item">
              <a
                href="{{route('product-list')}}"
                data-menu-path=""
                class="menu-item-link"
              >
                <div class="menu-item-icon">
                  <i class="fa fa-desktop"></i>
                </div>
                <span class="menu-item-text">Product List</span>
              </a>
            </div>
            
          </div>
          <!-- END Menu -->
        </div>
      </div>
      <!-- END Aside -->