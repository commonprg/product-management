@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<!-- BEGIN Page Content -->
<div class="content">
    <div class="container-fluid">
       <h2 class="mb-4"> Dashboard </h2>
        <div class="row">
            <div class="col-12">
                <!-- BEGIN Portlet -->
                <div class="portlet">
                    <div class="portlet-header portlet-header-bordered">
                        <h3 class="portlet-title">Products</h3>
                        <div class="col-md-6 add-new">
                            <div class="box" style="float: right;">
                                <a href="{{route('create')}}">
                                    <i style="font-size: 3em;" class="fa fa-3x fa-plus-circle"></i>
                                </a>
                            </div>
                        </div>
                        
                        
                    </div>

                    <div class="portlet-body">
                        
                        <!-- BEGIN Datatable -->
                        <table id="js-product-list" class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Discription</th>
                                    <th>Image</th>
                                    <th>Size</th>
                                    <th>Color</th>
                                    <th>Action</th>
                                                                                
                                </tr>
                            </thead>
                            <tbody>
                                <!-- data appending here -->
                            </tbody>
                        </table>
                        <!-- END Datatable -->
                    </div>
                </div>
                <!-- END Portlet -->
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection
@push('js')
<script type="text/javascript" src="{{asset('admin-theme/assets/app/product.js')}}"></script>


	
@endpush