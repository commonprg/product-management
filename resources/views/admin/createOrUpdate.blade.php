@extends('admin.layouts.app')
@section('headerClass','')
@section('content')
<!-- BEGIN Page Content -->
<div class="content">
    <div class="container-fluid">
       <h2 class="mb-4"> Dashboard </h2>
        <div class="row">
            <div class="col-12">
                <!-- BEGIN Portlet -->
                <div class="portlet">
                    <div class="portlet-header portlet-header-bordered">
                        <h3 class="portlet-title">Add Product</h3>
                        
                        
                    </div>

                    <div class="portlet-body">
                        
                        @if(isset($product))
                            {{ Form::model($product, ['route' => ['update', $product->id], 'method' => 'patch']) }}
                        @else
                            {{ Form::open(['route' => 'store']) }}
                        @endif

                        <div class="form-row">
                            <div class="col-sm-6">
                                <div class="form-group position-relative">
                                    <label class="portlet-title">Title <span class="text-danger">*</span></label>
                                    {{ Form::text('title', Input::old('title')) }}
                                    @error('name')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div><!--end col-->
                            <div class="col-sm-6">
                                <div class="form-group position-relative">
                                    <label class="portlet-title">Description <span class="text-danger">*</span></label>
                                    {{ Form::text('description', Input::old('description')) }}
                                    @error('email')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div> 
                            </div><!--end col-->
                            <div class="col-sm-6">
                                <div class="form-group position-relative">
                                    <label class="portlet-title">Size <span class="text-danger">*</span></label>
                                    {{ Form::text('size', Input::old('size')) }}
                                    @error('phone')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div><!--end col-->
                            <div class="col-sm-6">
                                <div class="form-group position-relative">
                                    <label class="portlet-title">Color <span class="text-danger">*</span></label>
                                    {{ Form::text('color', Input::old('color')) }}
                                    @error('state')
                                        <span class="invalid-feedback d-block" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div><!--end col-->
                            
                            <div class="col-md-12 text-center">
                            <hr>
                            {{ Form::submit('Save', ['name' => 'submit']) }}
                            </div>
                        </div>
                            
                        {{ Form::close() }}
                    </div>
                </div>
                <!-- END Portlet -->
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
@endsection
@push('js')
<script type="text/javascript" src="{{asset('admin-theme/assets/app/product.js')}}"></script>


	
@endpush